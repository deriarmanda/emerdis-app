package com.krakatio.derdevil.emerdisapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.krakatio.derdevil.emerdisapp.ui.auth.AuthActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuthenticationEspressoTest {

    @Rule
    var mActivityRule: ActivityScenarioRule<AuthActivity> =
        ActivityScenarioRule(AuthActivity::class.java)

    private val testedEmail: String = "abcde12345@gmail.com"
    private val testedPassword: String = "theEspressoTest"
    private val testedFirstName: String = "Emergency"
    private val testedLastName: String = "Disaster App"

    @Test
    fun fillSignInFields() {
        onView(withId(R.id.form_email)).perform(typeText(testedEmail), closeSoftKeyboard())
        onView(withId(R.id.form_password)).perform(typeText(testedPassword), closeSoftKeyboard())
    }

    @Test
    fun simulateSignIn() {
        onView(withId(R.id.form_email)).check(matches(withText(testedEmail)))
        onView(withId(R.id.form_password)).check(matches(withText(testedPassword)))
        onView(withId(R.id.button_signin)).perform(click()).check(matches(isClickable()))
    }

    @Test
    fun fillSignUpFields() {
        onView(withId(R.id.form_firstname)).perform(typeText(testedFirstName), closeSoftKeyboard())
        onView(withId(R.id.form_lastname)).perform(typeText(testedLastName), closeSoftKeyboard())
        onView(withId(R.id.form_email)).perform(typeText(testedEmail), closeSoftKeyboard())
        onView(withId(R.id.form_password)).perform(typeText(testedPassword), closeSoftKeyboard())
    }

    @Test
    fun simulateSignUp() {
        onView(withId(R.id.form_firstname)).check(matches(withText(testedFirstName)))
        onView(withId(R.id.form_lastname)).check(matches(withText(testedLastName)))
        onView(withId(R.id.form_email)).check(matches(withText(testedEmail)))
        onView(withId(R.id.form_password)).check(matches(withText(testedPassword)))
        onView(withId(R.id.button_signup)).perform(click()).check(matches(isClickable()))
    }
}

