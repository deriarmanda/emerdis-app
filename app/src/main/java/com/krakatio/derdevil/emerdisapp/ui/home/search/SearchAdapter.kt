package com.krakatio.derdevil.emerdisapp.ui.home.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.databinding.ItemSearchResultBinding

class SearchAdapter(
    private val onItemClick: (hospital: Hospital) -> Unit
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    var list = listOf<Hospital>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSearchResultBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hospital = list[position]
        holder.binding.hospital = hospital
        holder.binding.container.setOnClickListener { onItemClick(hospital) }
    }

    inner class ViewHolder(val binding: ItemSearchResultBinding) : RecyclerView.ViewHolder(binding.root)
}