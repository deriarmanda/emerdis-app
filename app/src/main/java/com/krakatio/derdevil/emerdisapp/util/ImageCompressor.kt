package com.krakatio.derdevil.emerdisapp.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream

object ImageCompressor {

    fun compress(path: String, format: Bitmap.CompressFormat): ByteArray {
        val image = BitmapFactory.decodeFile(path)
        val stream = ByteArrayOutputStream()
        image.compress(format, 100, stream)
        image.recycle()
        return stream.toByteArray()
    }
}