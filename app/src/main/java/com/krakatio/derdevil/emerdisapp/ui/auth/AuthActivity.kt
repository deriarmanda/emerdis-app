package com.krakatio.derdevil.emerdisapp.ui.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.ui.auth.signin.SignInFragment
import com.krakatio.derdevil.emerdisapp.ui.auth.signup.SignUpFragment
import com.krakatio.derdevil.emerdisapp.util.FRAGMENT_SIGN_IN
import com.krakatio.derdevil.emerdisapp.util.FRAGMENT_SIGN_UP
import com.krakatio.derdevil.emerdisapp.util.replaceFragmentByTag

class AuthActivity : AppCompatActivity() {

    private var currentTag = FRAGMENT_SIGN_IN

    companion object {
        private const val BUNDLE_CURRENT_TAG = "bundle_current_fragemnt_tag"
        fun intent(context: Context) = Intent(context, AuthActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        if (savedInstanceState != null) {
            currentTag = savedInstanceState.getString(BUNDLE_CURRENT_TAG) ?: FRAGMENT_SIGN_IN
        }

        val fragment =
            if (currentTag == FRAGMENT_SIGN_UP) {
                supportFragmentManager.findFragmentByTag(FRAGMENT_SIGN_UP) ?: SignUpFragment.instance()
            } else {
                supportFragmentManager.findFragmentByTag(FRAGMENT_SIGN_IN) ?: SignInFragment.instance()
            }
        replaceFragmentByTag(R.id.root, fragment, currentTag)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("fragment_tag", currentTag)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        if (savedInstanceState != null) {
            currentTag = savedInstanceState.getString(BUNDLE_CURRENT_TAG) ?: FRAGMENT_SIGN_IN
        }

        val fragment =
            if (currentTag == FRAGMENT_SIGN_UP) {
                supportFragmentManager.findFragmentByTag(FRAGMENT_SIGN_UP) ?: SignUpFragment.instance()
            } else {
                supportFragmentManager.findFragmentByTag(FRAGMENT_SIGN_IN) ?: SignInFragment.instance()
            }
        replaceFragmentByTag(R.id.root, fragment, currentTag)
    }

    override fun onBackPressed() {
        if (currentTag == FRAGMENT_SIGN_UP) navigateTo(FRAGMENT_SIGN_IN)
        else super.onBackPressed()
    }

    fun navigateTo(tag: String) {
        val fragment =
            supportFragmentManager.findFragmentByTag(tag) ?: if (tag == FRAGMENT_SIGN_UP) SignUpFragment.instance()
            else SignInFragment.instance()

        currentTag = tag
        replaceFragmentByTag(R.id.root, fragment, currentTag)
    }
}
