package com.krakatio.derdevil.emerdisapp.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.ui.about.AboutActivity
import com.krakatio.derdevil.emerdisapp.ui.account.AccountActivity
import com.krakatio.derdevil.emerdisapp.ui.auth.AuthActivity
import com.krakatio.derdevil.emerdisapp.ui.home.explore.ExploreFragment
import com.krakatio.derdevil.emerdisapp.ui.home.information.InformationFragment
import com.krakatio.derdevil.emerdisapp.ui.home.maps.MapsFragment
import com.krakatio.derdevil.emerdisapp.ui.home.search.SearchFragment
import com.krakatio.derdevil.emerdisapp.ui.home.welcome.WelcomeFragment
import com.krakatio.derdevil.emerdisapp.ui.triage.TriageActivity
import com.krakatio.derdevil.emerdisapp.util.*
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.app_bar_home.*

class HomeActivity :
    AppCompatActivity(),
    FirebaseAuth.AuthStateListener,
    NavigationView.OnNavigationItemSelectedListener,
    SearchFragment.OnSearchCompleteListener {

    companion object {
        fun intent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    private lateinit var navView: NavigationView
    private val mAuth = FirebaseAuth.getInstance()
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragment: Fragment?
        when (item.itemId) {
            R.id.nav_welcome -> {
                fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_WELCOME) ?: WelcomeFragment.instance()
                replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_WELCOME)
                navView.setCheckedItem(R.id.drawer_home)
                fab_find.hide()
//                fab_location.hide()
                search_view.visibility = View.GONE
                supportActionBar?.hide()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_maps -> {
                fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_MAPS) ?: MapsFragment.instance()
                replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_MAPS)
                navView.setCheckedItem(R.id.drawer_maps)
                fab_find.show()
//                fab_location.show()
                search_view.visibility = View.VISIBLE
                supportActionBar?.setTitle(R.string.title_maps)
                supportActionBar?.show()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_explore -> {
                fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_EXPLORE) ?: ExploreFragment.instance()
                replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_EXPLORE)
                navView.setCheckedItem(R.id.drawer_explore)
                fab_find.show()
//                fab_location.hide()
                search_view.visibility = View.VISIBLE
                supportActionBar?.setTitle(R.string.title_explore)
                supportActionBar?.show()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_information -> {
                fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_INFO) ?: InformationFragment.instance()
                replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_INFO)
                navView.setCheckedItem(R.id.drawer_info)
                fab_find.show()
//                fab_location.hide()
                search_view.visibility = View.VISIBLE
                supportActionBar?.setTitle(R.string.title_information)
                supportActionBar?.show()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab_find)
        fab.setOnClickListener { startActivity(TriageActivity.intent(this)) }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)
        bottom_nav.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        bottom_nav.selectedItemId = R.id.nav_welcome

        search_view.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
                closeSearchPage()
            }

            override fun onSearchViewShown() {
                openSearchPage()
            }
        })
        search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search_view.clearFocus()
                val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_SEARCH)
                return if (fragment is SearchFragment && query != null && query.isNotBlank()) {
                    fragment.processQuery(query)
                    true
                } else false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_SEARCH)
                return if (fragment is SearchFragment && newText != null && newText.isNotBlank()) {
                    fragment.processQuery(newText)
                    true
                } else false
            }
        })
    }

    override fun onStart() {
        super.onStart()
        mAuth.addAuthStateListener(this)
    }

    override fun onStop() {
        super.onStop()
        mAuth.removeAuthStateListener(this)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        when {
            drawerLayout.isDrawerOpen(GravityCompat.START) -> drawerLayout.closeDrawer(GravityCompat.START)
            search_view.isSearchOpen -> search_view.closeSearch()
            bottom_nav.selectedItemId != R.id.nav_welcome -> bottom_nav.selectedItemId = R.id.nav_welcome
            else -> super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_home_options, menu)
        search_view.setMenuItem(menu.findItem(R.id.action_search))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_account -> {
                startActivity(AccountActivity.intent(this))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        val user = auth.currentUser
        if (user != null) {
            val header = navView.getHeaderView(0)
            val textName: TextView = header.findViewById(R.id.text_name)
            val textEmail: TextView = header.findViewById(R.id.text_email)
            val imageProfile: CircleImageView = header.findViewById(R.id.image_profile)

            textName.text = user.displayName
            textEmail.text = user.email
            Picasso.get()
                .load(user.photoUrl)
                .placeholder(R.drawable.ic_account_circle_white_64dp)
                .error(R.drawable.ic_account_circle_white_64dp)
                .into(imageProfile)
        } else {
            Toast.makeText(this, R.string.home_msg_bye, Toast.LENGTH_LONG).show()
            startActivity(AuthActivity.intent(this))
            finish()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.drawer_home -> bottom_nav.selectedItemId = R.id.nav_welcome
            R.id.drawer_maps -> bottom_nav.selectedItemId = R.id.nav_maps
            R.id.drawer_explore -> bottom_nav.selectedItemId = R.id.nav_explore
            R.id.drawer_info -> bottom_nav.selectedItemId = R.id.nav_information
            R.id.drawer_about -> startActivity(AboutActivity.intent(this))
            R.id.drawer_share -> Toast.makeText(this, R.string.msg_feature_unavailable, Toast.LENGTH_LONG).show()
            R.id.drawer_rate -> Toast.makeText(this, R.string.msg_feature_unavailable, Toast.LENGTH_LONG).show()
            R.id.drawer_exit -> mAuth.signOut()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onSearchComplete() {
        search_view.closeSearch()
    }

    private fun openSearchPage() {
        val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_SEARCH) ?: SearchFragment.instance()
        replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_SEARCH)
//        fab_location.hide()
        fab_find.hide()
        bottom_nav.visibility = View.GONE
    }

    private fun closeSearchPage() {
        bottom_nav.selectedItemId = bottom_nav.selectedItemId
        bottom_nav.visibility = View.VISIBLE
    }

}
