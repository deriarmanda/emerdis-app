package com.krakatio.derdevil.emerdisapp.ui.auth.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krakatio.derdevil.emerdisapp.data.manager.UserManager
import com.krakatio.derdevil.emerdisapp.util.default

class SignUpViewModel private constructor(
    private val userManager: UserManager
) : ViewModel() {

    val firstName = MutableLiveData<String>().default("")
    val lastName = MutableLiveData<String>().default("")
    val email = MutableLiveData<String>().default("")
    val password = MutableLiveData<String>().default("")
    val isLoading = MutableLiveData<Boolean>().default(false)
    private val signUpStatus = MutableLiveData<SignUpStatus>().default(SignUpStatus())

    fun getSignUpStatus(): LiveData<SignUpStatus> = signUpStatus

    fun doSignUp() {
        isLoading.value = true
        userManager.signUp(
            fullname = "${firstName.value} ${lastName.value}",
            email = email.value!!,
            password = password.value!!
        ) { error ->
            isLoading.value = false
            signUpStatus.value = currentStatus().copy(
                isSuccess = error == null,
                errorMessage = error
            )
        }
    }

    private fun currentStatus() = signUpStatus.value!!

    data class SignUpStatus(
        var isSuccess: Boolean = false,
        var errorMessage: String? = null
    )

    class Factory(
        private val userManager: UserManager
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SignUpViewModel(userManager) as T
        }
    }
}
