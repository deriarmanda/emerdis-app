package com.krakatio.derdevil.emerdisapp.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude

data class Information(
    var title: String = "",
    var content: String = "",
    var imageUrl: String = "",
    var sourceUrl: String = "",
    @get:Exclude var uid: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(content)
        parcel.writeString(imageUrl)
        parcel.writeString(sourceUrl)
        parcel.writeString(uid)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Information> {
        override fun createFromParcel(parcel: Parcel): Information {
            return Information(parcel)
        }

        override fun newArray(size: Int): Array<Information?> {
            return arrayOfNulls(size)
        }
    }
}