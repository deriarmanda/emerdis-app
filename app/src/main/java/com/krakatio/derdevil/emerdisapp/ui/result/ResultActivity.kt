package com.krakatio.derdevil.emerdisapp.ui.result

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.manager.HistoryManager
import com.krakatio.derdevil.emerdisapp.data.model.History
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.databinding.ActivityResultBinding
import com.krakatio.derdevil.emerdisapp.util.Injectors
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        private const val EXTRA_HISTORY = "extra_history"
        private const val EXTRA_HOSPITALS = "extra_list_hospital"
        fun intent(context: Context, history: History, list: ArrayList<Hospital>): Intent {
            val intent = Intent(context, ResultActivity::class.java)
            intent.putExtra(EXTRA_HISTORY, history)
            intent.putParcelableArrayListExtra(EXTRA_HOSPITALS, list)
            return intent
        }
    }

    private lateinit var mBinding: ActivityResultBinding
    private lateinit var mMap: GoogleMap
    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var mHistory: History
    private lateinit var mHospitalList: List<Hospital>
    private lateinit var mHistoryManager: HistoryManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_result)
        mHistory = intent.getParcelableExtra(EXTRA_HISTORY)
        mHospitalList = intent.getParcelableArrayListExtra(EXTRA_HOSPITALS)

        mBinding.hospital1 = mHospitalList[0]
        mBinding.hospital2 = mHospitalList[1]
        mBinding.hospital3 = mHospitalList[2]
        mBinding.activity = this

        Picasso.get()
            .load(mHospitalList[0].imageUrls.split(";")[0])
            .placeholder(R.drawable.img_placeholder)
            .error(R.drawable.img_placeholder)
            .into(mBinding.imageHospital1)
        Picasso.get()
            .load(mHospitalList[1].imageUrls.split(";")[0])
            .placeholder(R.drawable.img_placeholder)
            .error(R.drawable.img_placeholder)
            .into(mBinding.imageHospital2)
        Picasso.get()
            .load(mHospitalList[2].imageUrls.split(";")[0])
            .placeholder(R.drawable.img_placeholder)
            .error(R.drawable.img_placeholder)
            .into(mBinding.imageHospital3)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        mHistoryManager = Injectors.getHistoryManager()

        mBinding.textTitle.setOnClickListener {
            if (mBottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ContextCompat.checkSelfPermission(
                applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mMap.isMyLocationEnabled = true
        }

        val bounds = LatLngBounds.Builder()
        for ((index, hospital) in mHospitalList.withIndex()) {
            val location = LatLng(hospital.latitude, hospital.longitude)
            val marker = MarkerOptions()
                .title(hospital.name)
                .snippet("Rekomendasi ke ${index + 1}")
                .position(location)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_hospital_round_32dp))
            mMap.addMarker(marker).tag = hospital
            bounds.include(location)
        }

        mMap.isTrafficEnabled = true
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 24))
        mMap.setOnCameraMoveStartedListener { mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED }
        mMap.setOnMarkerClickListener { marker ->
            marker.showInfoWindow()
            mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            val hospital = marker.tag as Hospital
            val index = when {
                hospital.name == mHospitalList[2].name -> 3
                hospital.name == mHospitalList[1].name -> 2
                else -> 1
            }

            mBinding.textNumber1.setBackgroundResource(
                if (index == 1) R.color.primary
                else R.color.accent
            )
            mBinding.textNumber2.setBackgroundResource(
                if (index == 2) R.color.primary
                else R.color.accent
            )
            mBinding.textNumber3.setBackgroundResource(
                if (index == 3) R.color.primary
                else R.color.accent
            )

            return@setOnMarkerClickListener true
        }

        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun openGoogleNavigations(hospital: Hospital) {
        mHistory.uidHospital = hospital.uid
        mHistory.hospital = hospital
        mHistoryManager.updateHistory(mHistory)

        val gmmIntentUri = Uri.parse("google.navigation:q=${hospital.latitude},${hospital.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
