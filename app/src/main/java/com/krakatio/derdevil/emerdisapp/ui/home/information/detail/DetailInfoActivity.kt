package com.krakatio.derdevil.emerdisapp.ui.home.information.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.model.Information
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_info.*

class DetailInfoActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_INFO = "extra_information"
        fun intent(context: Context, info: Information): Intent {
            val intent = Intent(context, DetailInfoActivity::class.java)
            intent.putExtra(EXTRA_INFO, info)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_info)

        val info: Information = intent.getParcelableExtra(EXTRA_INFO)
        text_title.text = info.title
        text_content.text = info.content.replace("_n", "\n")
        text_source.text = info.sourceUrl
        Picasso.get().load(info.imageUrl).into(image_info)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }
}
