package com.krakatio.derdevil.emerdisapp.ui.auth.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.databinding.FragmentSignUpBinding
import com.krakatio.derdevil.emerdisapp.ui.auth.AuthActivity
import com.krakatio.derdevil.emerdisapp.ui.home.HomeActivity
import com.krakatio.derdevil.emerdisapp.util.Injectors

class SignUpFragment : Fragment() {

    companion object {
        fun instance() = SignUpFragment()
    }

    private val viewModel: SignUpViewModel by lazy {
        val factory = Injectors.provideSignUpViewModelFactory()
        ViewModelProviders.of(this, factory).get(SignUpViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentSignUpBinding>(
            inflater,
            R.layout.fragment_sign_up,
            container,
            false
        )
        binding.viewModel = viewModel
        binding.authActivity = activity as AuthActivity
        binding.lifecycleOwner = viewLifecycleOwner
        binding.executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getSignUpStatus().observe(viewLifecycleOwner, Observer { status ->
            if (status.isSuccess) onSignUpSuccess()
            status.errorMessage?.let { onSignUpFailed(it) }
        })
    }

    private fun onSignUpSuccess() {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            Toast.makeText(requireContext(), getString(R.string.home_msg_welcome, user.displayName), Toast.LENGTH_LONG)
                .show()
        }
        startActivity(HomeActivity.intent(requireContext()))
        requireActivity().finish()
    }

    private fun onSignUpFailed(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }
}
