package com.krakatio.derdevil.emerdisapp.ui.hospital

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.krakatio.derdevil.emerdisapp.R
import com.smarteist.autoimageslider.SliderViewAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_image_slider.view.*

class HospitalSliderAdapter(
    private val imageUrls: List<String>,
    private val onImageClick: (position: Int) -> Unit
) : SliderViewAdapter<HospitalSliderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        val view = LayoutInflater.from(parent?.context)
            .inflate(R.layout.item_image_slider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, position: Int) {
        viewHolder?.bind(imageUrls[position], position)
    }

    override fun getCount(): Int {
        return imageUrls.size
    }

    inner class ViewHolder(private val view: View) : SliderViewAdapter.ViewHolder(view) {
        fun bind(url: String, position: Int) {
            Picasso.get()
                .load(url)
                .placeholder(R.drawable.img_placeholder)
                .error(R.drawable.img_placeholder)
                .into(view.image)
            view.image.setOnClickListener { onImageClick(position) }
        }
    }
}