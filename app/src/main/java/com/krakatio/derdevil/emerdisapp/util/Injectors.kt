package com.krakatio.derdevil.emerdisapp.util

import com.krakatio.derdevil.emerdisapp.data.AhpEngine
import com.krakatio.derdevil.emerdisapp.data.manager.HistoryManager
import com.krakatio.derdevil.emerdisapp.data.manager.HospitalManager
import com.krakatio.derdevil.emerdisapp.data.manager.UserManager
import com.krakatio.derdevil.emerdisapp.ui.account.AccountViewModel
import com.krakatio.derdevil.emerdisapp.ui.auth.signin.SignInViewModel
import com.krakatio.derdevil.emerdisapp.ui.auth.signup.SignUpViewModel
import com.krakatio.derdevil.emerdisapp.ui.home.explore.ExploreViewModel
import com.krakatio.derdevil.emerdisapp.ui.triage.TriageViewModel

object Injectors {

    fun provideSignInViewModelFactory(): SignInViewModel.Factory {
        return SignInViewModel.Factory(getUserManager())
    }

    fun provideSignUpViewModelFactory(): SignUpViewModel.Factory {
        return SignUpViewModel.Factory(getUserManager())
    }

    fun provideExploreViewModelFactory(): ExploreViewModel.Factory {
        return ExploreViewModel.Factory(getHospitalManager())
    }

    fun provideAccountViewModelFactory(): AccountViewModel.Factory {
        return AccountViewModel.Factory(getHistoryManager())
    }

    fun provideTriageViewModelFactory(): TriageViewModel.Factory {
        return TriageViewModel.Factory(getAhpEngine(), getHistoryManager())
    }

    fun getHospitalManager(): HospitalManager {
        return HospitalManager.getInstance()
    }

    fun getHistoryManager(): HistoryManager {
        return HistoryManager.getInstance()
    }

    private fun getUserManager(): UserManager {
        return UserManager.getInstance()
    }

    private fun getAhpEngine(): AhpEngine {
        return AhpEngine.getInstance(getHospitalManager())
    }

}