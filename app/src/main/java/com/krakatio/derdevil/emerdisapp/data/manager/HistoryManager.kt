package com.krakatio.derdevil.emerdisapp.data.manager

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.krakatio.derdevil.emerdisapp.data.model.History
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.util.default

class HistoryManager private constructor() {

    private val mDbRef = FirebaseDatabase.getInstance().getReference("histories")
    private val mHistoryList = MutableLiveData<List<History>>().default(emptyList())

    companion object {
        @Volatile
        private var instance: HistoryManager? = null

        fun getInstance(): HistoryManager {
            return instance ?: synchronized(this) {
                instance ?: HistoryManager().also { instance = it }
            }
        }
    }

    init {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            val childRef = mDbRef.child(user.uid).orderByChild("dateTime")
            childRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {}
                override fun onDataChange(snapshot: DataSnapshot) {
                    val list = arrayListOf<History>()
                    for (child in snapshot.children) {
                        val history = child.getValue(History::class.java)
                        if (history != null) {
                            val hospitalRef = FirebaseDatabase.getInstance()
                                .getReference("hospitals")
                                .child(history.uidHospital)
                            hospitalRef.addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onCancelled(dbError: DatabaseError) {}
                                override fun onDataChange(snapshot: DataSnapshot) {
                                    val hospital = snapshot.getValue(Hospital::class.java)
                                    if (hospital != null) {
                                        history.hospital = hospital
                                        list.add(history)
                                        mHistoryList.postValue(list)
                                    }
                                }
                            })
                        }
                    }
                    mHistoryList.postValue(list)
                }
            })
        }
    }

    fun getHistorylList(): LiveData<List<History>> = mHistoryList

    fun reloadList(onComplete: (errorMsg: String?) -> Unit) {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            val childRef = mDbRef.child(user.uid).orderByChild("dateTime")
            childRef.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    onComplete(dbError.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val list = arrayListOf<History>()
                    for (child in snapshot.children) {
                        val history = child.getValue(History::class.java)
                        if (history != null) {
                            val hospitalRef = FirebaseDatabase.getInstance()
                                .getReference("hospitals")
                                .child(history.uidHospital)
                            hospitalRef.addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onCancelled(dbError: DatabaseError) {}
                                override fun onDataChange(snapshot: DataSnapshot) {
                                    val hospital = snapshot.getValue(Hospital::class.java)
                                    if (hospital != null) {
                                        history.hospital = hospital
                                        list.add(history)
                                        mHistoryList.value = list
                                    }
                                }
                            })
                        }
                    }
                    mHistoryList.value = list
                    onComplete(null)
                }
            })
        }
    }

    fun saveHistory(history: History, onComplete: (history: History, errorMsg: String?) -> Unit) {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            val childRef = mDbRef.child(user.uid)
            val key = childRef.push().key
            if (key != null) {
                childRef.child(key).setValue(history).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val data = history.copy(uid = key)
                        onComplete(data, null)
                    } else onComplete(history, it.exception?.message!!)
                }
            } else onComplete(history, "Tidak ada koneksi internet!")
        }
    }

    fun updateHistory(history: History) {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            val childRef = mDbRef.child(user.uid).child(history.uid)
            childRef.setValue(history)
        }
    }
}