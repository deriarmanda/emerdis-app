package com.krakatio.derdevil.emerdisapp.ui.home.more

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.krakatio.derdevil.emerdisapp.R
import kotlinx.android.synthetic.main.fragment_menu_list_dialog.*
import kotlinx.android.synthetic.main.fragment_menu_list_dialog_item.view.*

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    MenuListDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 * You activity (or fragment) needs to implement [MenuListDialogFragment.MenuListener].
 */
class MenuListDialogFragment : BottomSheetDialogFragment() {
    private var mListener: MenuListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setOnShowListener {
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?

            val behavior = BottomSheetBehavior.from(bottomSheet!!)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = 0
            behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(p0: View, p1: Float) {}
                override fun onStateChanged(p0: View, state: Int) {
                    if (state == BottomSheetBehavior.STATE_COLLAPSED) dismiss()
                }
            })
        }

        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_menu_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = MenuAdapter(
            listOf(
                Pair(getString(R.string.title_home), R.drawable.ic_home_black_24dp),
                Pair(getString(R.string.title_maps), R.drawable.ic_map_black_24dp),
                Pair(getString(R.string.title_explore), R.drawable.ic_view_list_black_24dp),
                Pair(getString(R.string.title_information), R.drawable.ic_local_library_black_24dp),
                Pair(getString(R.string.title_about), R.drawable.ic_info_black_24dp),
                Pair(getString(R.string.action_share), R.drawable.ic_share_black_24dp),
                Pair(getString(R.string.action_rate), R.drawable.ic_star_black_24dp),
                Pair(getString(R.string.action_exit), R.drawable.ic_exit_to_app_black_24dp)
            )
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener =
            if (parent != null) {
                parent as MenuListener
            } else {
                context as MenuListener
            }
    }

    override fun dismiss() {
        super.dismiss()
        mListener?.onMenuDismissed()
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        mListener?.onMenuDismissed()
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    interface MenuListener {
        fun onMenuClicked(position: Int)
        fun onMenuDismissed()
    }

    private inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_menu_list_dialog_item, parent, false)) {

        internal val text: TextView = itemView.text

        init {
            text.setOnClickListener {
                mListener?.let {
                    it.onMenuClicked(adapterPosition)
                    dismiss()
                }
            }
        }
    }

    private inner class MenuAdapter internal constructor(private val list: List<Pair<String, Int>>) :
        RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.text.text = list[position].first
            holder.text.setCompoundDrawablesRelativeWithIntrinsicBounds(list[position].second, 0, 0, 0)
        }

        override fun getItemCount(): Int {
            return list.size
        }
    }

    companion object {
        fun instance() = MenuListDialogFragment()
    }
}
