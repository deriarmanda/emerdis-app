package com.krakatio.derdevil.emerdisapp.ui.home.maps

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_map_info_window.view.*

class MapsAdapter(private val context: Context) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }

    @SuppressLint("InflateParams")
    override fun getInfoContents(marker: Marker): View {
        val view = LayoutInflater.from(context).inflate(R.layout.item_map_info_window, null)
        val hospital = marker.tag as Hospital

        Picasso.get().load(hospital.imageUrls.split(";")[0]).into(view.image_hospital)
        view.text_name.text = hospital.name
        view.text_address.text = hospital.address
        view.text_class.text = context.getString(
            R.string.hospital_text_class_level,
            hospital.classRank,
            hospital.igdLevel
        )

        return view
    }

}