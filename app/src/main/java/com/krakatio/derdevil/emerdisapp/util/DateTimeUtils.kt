package com.krakatio.derdevil.emerdisapp.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    private const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm"
    private const val DAY_DATE_PATTERN = "EEEE, d MMM yyyy"
    private const val DATE_PATTERN = "yyyy-MM-dd"
    private const val TIME_PATTERN = "HH:mm"
    private const val HISTORY_PATTERN = "$TIME_PATTERN, d MMMM yyyy"
    private val DATE_TIME_FORMATTER = SimpleDateFormat(DATE_TIME_PATTERN, Locale.ENGLISH)
    private val DAY_DATE_FORMATTER = SimpleDateFormat(DAY_DATE_PATTERN, Locale.ENGLISH)
    private val HISTORY_FORMATTER = SimpleDateFormat(HISTORY_PATTERN, Locale.ENGLISH)
    private val DATE_FORMATTER = SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH)
    private val TIME_FORMATTER = SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH)

    fun getCurrentDateTimeStringForDb(): String {
        return DATE_TIME_FORMATTER.format(Date())
    }

    fun parseForHistory(dateTime: String): String {
        val converted = parseDateTime(dateTime)
        return HISTORY_FORMATTER.format(converted)
    }

    private fun parseDateTime(dateTime: String) = DATE_TIME_FORMATTER.parse(dateTime)
}