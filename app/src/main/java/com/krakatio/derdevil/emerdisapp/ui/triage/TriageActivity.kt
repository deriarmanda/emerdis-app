package com.krakatio.derdevil.emerdisapp.ui.triage

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.google.android.gms.location.*
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.databinding.ActivityTriageBinding
import com.krakatio.derdevil.emerdisapp.ui.result.ResultActivity
import com.krakatio.derdevil.emerdisapp.util.Injectors
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_triage_result.*
import java.io.File

class TriageActivity : AppCompatActivity() {

    companion object {
        private const val RC_OPEN_CAMERA = 111
        fun intent(context: Context) = Intent(context, TriageActivity::class.java)
    }

    private val viewModel: TriageViewModel by lazy {
        val factory = Injectors.provideTriageViewModelFactory()
        ViewModelProviders.of(this, factory).get(TriageViewModel::class.java)
    }
    private lateinit var locationService: FusedLocationProviderClient
    private lateinit var loadingDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityTriageBinding = DataBindingUtil.setContentView(this, R.layout.activity_triage)
        binding.activity = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        binding.detectionPage.imageSwitcher.setFactory {
            val image = ImageView(this)
            image.layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            image.scaleType = ImageView.ScaleType.FIT_XY
            image
        }

        locationService = LocationServices.getFusedLocationProviderClient(this)
        observeUserLocation { viewModel.userLocation = it }

        viewModel.viewState.observe(this, Observer { state ->
            if (state.isLoading) showLoading()
            state.errorMessage?.let { showError(it) }
            binding.viewFlipper.displayedChild = state.page
            binding.detectionPage.imageSwitcher.setImageResource(state.step.questionDrawableRes)
            binding.resultPage.cardResult.setCardBackgroundColor(
                ResourcesCompat.getColor(
                    resources,
                    state.statusCardColorRes,
                    theme
                )
            )
            binding.resultPage.textResult.setTextColor(
                ResourcesCompat.getColor(
                    resources,
                    state.statusTextColorRes,
                    theme
                )
            )
        })
        viewModel.getResult().observe(this, Observer {
            loadingDialog.dismiss()
            startActivity(ResultActivity.intent(this, it.history, it.hospitals))
            finish()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_OPEN_CAMERA && resultCode == Activity.RESULT_OK) {
            val image = ImagePicker.getFirstImageOrNull(data)
            viewModel.setPhotoPath(image.path)
            Picasso.get()
                .load(Uri.fromFile(File(image.path)))
                .resize(image_problem.width, image_problem.height)
                .into(image_problem)
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun observeUserLocation(callback: (location: Location) -> Unit) {
        if (ContextCompat.checkSelfPermission(
                applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val request = LocationRequest.create().apply {
                interval = 10000
                fastestInterval = 5000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
            locationService.locationAvailability.addOnSuccessListener { availability ->
                if (availability != null && availability.isLocationAvailable) {
                    locationService.lastLocation.addOnSuccessListener { if (it != null) callback(it) }
                } else {
                    val locationCallback = object : LocationCallback() {
                        override fun onLocationResult(result: LocationResult?) {
                            if (result != null) callback(result.lastLocation)
                        }
                    }
                    locationService.requestLocationUpdates(request, locationCallback, null).addOnSuccessListener {
                        locationService.lastLocation.addOnSuccessListener { if (it != null) callback(it) }
                    }
                }
            }
        }
    }

    fun openCamera() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL)
            .single()
            .toolbarImageTitle("Ambil Foto Kondisi Pasien")
            .theme(R.style.AppTheme_CustomImagePicker)
            .start(RC_OPEN_CAMERA)
    }

    private fun showLoading() {
        loadingDialog = AlertDialog.Builder(this)
            .setView(R.layout.dialog_loading_triage)
            .setCancelable(false)
            .create()
        loadingDialog.show()
    }

    private fun showError(message: String) {
        loadingDialog.dismiss()
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        viewModel.onErrorHandled()
    }
}
