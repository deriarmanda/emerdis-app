package com.krakatio.derdevil.emerdisapp.ui.home.information


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.ui.home.information.detail.DetailInfoActivity
import kotlinx.android.synthetic.main.fragment_information.*

/**
 * A simple [Fragment] subclass.
 *
 */
class InformationFragment : Fragment() {

    companion object {
        fun instance() = InformationFragment()
    }

    private val viewModel: InformationViewModel by lazy {
        ViewModelProviders.of(requireActivity()).get(InformationViewModel::class.java)
    }
    private val adapter = InformationAdapter {
        startActivity(DetailInfoActivity.intent(requireContext(), it))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_info.adapter = adapter
        with(swipe_refresh) {
            setColorSchemeResources(R.color.accent)
            setOnRefreshListener { viewModel.fetchList() }
        }

        viewModel.getList().observe(viewLifecycleOwner, Observer { adapter.list = it })
        viewModel.getViewState().observe(viewLifecycleOwner, Observer { state ->
            swipe_refresh.isRefreshing = state.isLoading
            state.errorMessage?.let { handleError(it) }
        })
    }

    private fun handleError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
        viewModel.onErrorHandled()
    }
}
