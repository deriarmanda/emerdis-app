package com.krakatio.derdevil.emerdisapp.ui.auth.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.databinding.FragmentSignInBinding
import com.krakatio.derdevil.emerdisapp.ui.auth.AuthActivity
import com.krakatio.derdevil.emerdisapp.ui.home.HomeActivity
import com.krakatio.derdevil.emerdisapp.util.Injectors

class SignInFragment : Fragment() {

    companion object {
        fun instance() = SignInFragment()
    }

    private val viewModel: SignInViewModel by lazy {
        val factory = Injectors.provideSignInViewModelFactory()
        ViewModelProviders.of(this, factory).get(SignInViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentSignInBinding>(
            inflater,
            R.layout.fragment_sign_in,
            container,
            false
        )
        binding.viewModel = viewModel
        binding.authActivity = activity as AuthActivity
        binding.lifecycleOwner = viewLifecycleOwner
        binding.executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getSignInStatus().observe(viewLifecycleOwner, Observer { status ->
            if (status.isSuccess) onSignInSuccess()
            status.errorMessage?.let { onSignInFailed(it) }
        })
    }

    private fun onSignInSuccess() {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            Toast.makeText(requireContext(), getString(R.string.home_msg_welcome, user.displayName), Toast.LENGTH_LONG)
                .show()
        }
        startActivity(HomeActivity.intent(requireContext()))
        requireActivity().finish()
    }

    private fun onSignInFailed(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }
}
