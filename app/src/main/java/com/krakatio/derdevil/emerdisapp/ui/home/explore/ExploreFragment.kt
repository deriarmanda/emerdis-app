package com.krakatio.derdevil.emerdisapp.ui.home.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.ui.hospital.HospitalActivity
import com.krakatio.derdevil.emerdisapp.util.Injectors
import kotlinx.android.synthetic.main.fragment_explore.*

class ExploreFragment : Fragment() {

    companion object {
        fun instance() = ExploreFragment()
    }

    private val viewModel: ExploreViewModel by lazy {
        val factory = Injectors.provideExploreViewModelFactory()
        ViewModelProviders.of(requireActivity(), factory).get(ExploreViewModel::class.java)
    }
    private val adapter = ExploreAdapter {
        startActivity(HospitalActivity.intent(requireContext(), it))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_explore, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_hospital.adapter = adapter
        with(swipe_refresh) {
            setColorSchemeResources(R.color.accent)
            setOnRefreshListener { viewModel.reloadList() }
        }

        viewModel.getList().observe(viewLifecycleOwner, Observer { adapter.list = it })
        viewModel.getViewState().observe(viewLifecycleOwner, Observer { state ->
            swipe_refresh.isRefreshing = state.isLoading
            state.errorMessage?.let { handleError(it) }
        })
    }

    private fun handleError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
        viewModel.onErrorHandled()
    }
}
