package com.krakatio.derdevil.emerdisapp.ui.home.information

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.emerdisapp.data.model.Information
import com.krakatio.derdevil.emerdisapp.databinding.ItemInformationBinding

class InformationAdapter(
    private val onItemClick: (info: Information) -> Unit
) : RecyclerView.Adapter<InformationAdapter.ViewHolder>() {

    var list = listOf<Information>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemInformationBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.info = list[position]
        holder.binding.container.setOnClickListener { onItemClick(list[position]) }
    }

    inner class ViewHolder(val binding: ItemInformationBinding) : RecyclerView.ViewHolder(binding.root)
}