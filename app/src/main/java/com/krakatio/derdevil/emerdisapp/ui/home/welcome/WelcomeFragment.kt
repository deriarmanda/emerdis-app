package com.krakatio.derdevil.emerdisapp.ui.home.welcome


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.ui.triage.TriageActivity
import kotlinx.android.synthetic.main.fragment_welcome.*

/**
 * A simple [Fragment] subclass.
 *
 */
class WelcomeFragment : Fragment() {

    companion object {
        fun instance() = WelcomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_triage.setOnClickListener { startActivity(TriageActivity.intent(requireContext())) }
    }
}
