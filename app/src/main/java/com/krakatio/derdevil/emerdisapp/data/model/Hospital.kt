package com.krakatio.derdevil.emerdisapp.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude

data class Hospital(
    var name: String = "",
    var address: String = "",
    var type: String = "",
    var classRank: String = "",
    var igdLevel: String = "",
    var phone: String = "",
    var website: String = "",
    var description: String = "",
    var visitors: Long = 0,
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var imageUrls: String = "",
    @get:Exclude var uid: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readLong(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeString(type)
        parcel.writeString(classRank)
        parcel.writeString(igdLevel)
        parcel.writeString(phone)
        parcel.writeString(website)
        parcel.writeString(description)
        parcel.writeLong(visitors)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(imageUrls)
        parcel.writeString(uid)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hospital> {
        override fun createFromParcel(parcel: Parcel): Hospital {
            return Hospital(parcel)
        }

        override fun newArray(size: Int): Array<Hospital?> {
            return arrayOfNulls(size)
        }
    }
}