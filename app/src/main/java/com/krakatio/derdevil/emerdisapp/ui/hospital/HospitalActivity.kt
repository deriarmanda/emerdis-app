package com.krakatio.derdevil.emerdisapp.ui.hospital

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.databinding.ActivityHospitalBinding
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.squareup.picasso.Picasso
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.activity_hospital.*

class HospitalActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_HOSPITAL = "extra_hospital"
        fun intent(context: Context, hospital: Hospital): Intent {
            val intent = Intent(context, HospitalActivity::class.java)
            intent.putExtra(EXTRA_HOSPITAL, hospital)
            return intent
        }
    }

    private lateinit var hospital: Hospital

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hospital = intent.getParcelableExtra(EXTRA_HOSPITAL)

        val binding: ActivityHospitalBinding = DataBindingUtil.setContentView(this, R.layout.activity_hospital)
        binding.activity = this
        binding.hospital = hospital
        binding.lifecycleOwner = this

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val urls = hospital.imageUrls.split(";")
        val adapter = HospitalSliderAdapter(urls) { fullscreenImage(it) }
        with(slider) {
            sliderAdapter = adapter
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            setIndicatorAnimation(IndicatorAnimations.WORM)
            startAutoCycle()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    fun makeACall() {
        val phone = hospital.phone
        if (phone == "-") {
            Toast.makeText(this, R.string.hospital_msg_no_phone, Toast.LENGTH_LONG).show()
        } else {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phone")
            startActivity(intent)
        }
    }

    fun openMapsDirection() {
        val gmmIntentUri = Uri.parse("google.navigation:q=${hospital.latitude},${hospital.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }

    fun openWebsite() {
        val website = hospital.website
        if (website == "-") {
            Toast.makeText(this, R.string.hospital_msg_no_website, Toast.LENGTH_LONG).show()
        } else {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(website))
            startActivity(intent)
        }
    }

    private fun fullscreenImage(position: Int) {
        val urls = hospital.imageUrls.split(";")
        val builder = StfalconImageViewer.Builder<String>(this, urls) { view, url ->
            Picasso.get().load(url).into(view)
        }
        builder.withStartPosition(position).show()
    }

//    dummy data
//    hospital = Hospital(
//    "RSUD Dr. Saiful Anwar Malang",
//    "Jl. Jaksa Agung Suprapto No. 2",
//    "RSU",
//    "A",
//    "IV",
//    "0341362101",
//    "https://rsusaifulanwar.jatimprov.go.id/",
//    "Sebelum perang dunia ke II, RSUD Dr. Saiful Anwar (pada waktu itu bernama Rumah Sakit Celaket), merupakan rumah sakit militer KNIL, yang pada pendudukan Jepang diambil alih oleh Jepang dan tetap digunakan sebagai rumah sakit militer. Pada saat perang kemerdekaan RI, Rumah Sakit Celaket dipakai sebagai rumah sakit tentara, sementara untuk umum digunakan Rumah Sakit Sukun yang ada dibawah Kotapraja Malang pada saat itu. Tahun 1947 (saat perang dunia ke II), karena keadaan bangunan yang lebih baik dan lebih muda, serta untuk kepentingan strategi militer, rumah sakit Sukun diambil alih oleh tentara pendudukan dan dijadikan rumah sakit militer, sedangkan Rumah Sakit Celaket dijadikan rumah sakit umum.\n" +
////    "\n\n" +
////    "Pada tanggal 14 September 1963, Yayasan Perguruan Tinggi Jawa Timur / IDI membuka Sekolah Tinggi Kedokteran Malang dan memakai Rumah Sakit Celaket sebagai tempat praktek (Program Kerjasama STKM-RS Celaket tanggal 23 Agustus 1969). Tanggal 2 Januari 1974, dengan Surat Keputusan Menteri Pendidikan dan Kebudayaan RI N0. 001/0/1974, Sekolah Tinggi Kedokteran Malang dijadikan Fakultas Kedokteran Universitas Brawijaya Malang, dengan Rumah Sakit Celaket sebagai tempat praktek.\n" +
////    "\n\n" +
////    "Pada tanggal 12 Nopember 1979, oleh Gubernur Kepala Daerah Tingkat I Jawa Timur, Rumah Sakit Celaket diresmikan sebagai Rumah Sakit Umum Daerah Dr. Saiful Anwar. Keputusan Menteri Kesehatan RI No. 51/Menkes/SK/III/1979 tanggal 22 Pebruari 1979, menetapkan RSUD Dr. Saiful Anwar sebagai rumah sakit rujukan. Pada tahun 2002 Berdasarkan PERDA No. 23 Tahun 2002 RSU Saiful Anwar ditetapkan sebagai Unsur Penunjang Pemerintah Provinsi setingkat dengan Badan. Pada bulan April 2007 dengan Keputusan Menteri Kesehatan RI No.673/MENKES/SK/VI/2007 RSUD Dr. Saiful Anwar ditetapkan sebagai Rumah Sakit kelas A. Pada tanggal 30 Desember 2008 ditetapkan sebagai Badan Layanan Umum (BLU) dengan keputusan Gubernur Jawa Timur No. 188/439/KPTS/013/2008. Pada tanggal 20 Januari tahun 2011 RSUD Dr. Saiful Anwar ditetapkan sebagai Rumah Sakit Pendidikan Utama Akreditasi A melalui sertifikat dari Kementerian Kesehatan RI dengan Nomor Sertifikat 123/MENKES/SK/I/2011.\n" +
////    "\n\n" +
////    "Terakhir pada tanggal 16 Maret 2015 RSUD Dr. Saiful Anwar ditetapkan telah Terakreditasi KARS Versi 2012 dengan menerima Sertifikat Lulus Tingkat PARIPURNA yang diberikan oleh KOMISI AKREDITASI RUMAH SAKIT (KARS) dengan NOMOR : KARS-SERT/95/III/2015 dengan masa berlaku mulai tanggal 23 Maret 2015 s/d 23 Februari 2018.",
//    -7.971794,
//    112.631611,
//    "http://rsusaifulanwar.jatimprov.go.id/wp-content/uploads/2016/09/rsud-dr-saiful-anwar.jpg" +
//    ";http://rsusaifulanwar.jatimprov.go.id/wp-content/uploads/2016/09/rssa.jpg" +
//    ";http://rsusaifulanwar.jatimprov.go.id/wp-content/uploads/2016/09/1.jpg" +
//    ";http://rsusaifulanwar.jatimprov.go.id/wp-content/uploads/2016/09/rumah-sakit-saiful-anwar-1930-681x315-1-681x315.jpg",
//    ""
//    )
}
