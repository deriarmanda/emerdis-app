package com.krakatio.derdevil.emerdisapp.ui.home.maps


import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.ui.hospital.HospitalActivity
import com.krakatio.derdevil.emerdisapp.util.Injectors

/**
 * A simple [Fragment] subclass.
 *
 */
class MapsFragment : Fragment(), OnMapReadyCallback {

    companion object {
        fun instance() = MapsFragment()
    }

    private lateinit var mMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.hospital_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mMap.isMyLocationEnabled = true
        }

        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.setInfoWindowAdapter(MapsAdapter(requireContext()))
        mMap.setOnInfoWindowClickListener { marker ->
            val hospital = marker.tag as Hospital
            startActivity(HospitalActivity.intent(requireContext(), hospital))
        }

        val manager = Injectors.getHospitalManager()
        manager.getHospitalList().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val bounds = LatLngBounds.Builder()
                for (hospital in it) {
                    val location = LatLng(hospital.latitude, hospital.longitude)
                    val marker = MarkerOptions()
                        .title(hospital.name)
                        .position(location)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_hospital_round_32dp))
                    mMap.addMarker(marker).tag = hospital
                    bounds.include(location)
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 24))
            }
        })
    }

}
