package com.krakatio.derdevil.emerdisapp.ui.account

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.databinding.ActivityAccountBinding
import com.krakatio.derdevil.emerdisapp.util.Injectors
import com.squareup.picasso.Picasso
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.activity_account.*

class AccountActivity : AppCompatActivity() {

    private val viewModel: AccountViewModel by lazy {
        val factory = Injectors.provideAccountViewModelFactory()
        ViewModelProviders.of(this, factory).get(AccountViewModel::class.java)
    }
    private val adapter = AccountAdapter(
        { openMapsDirection(it.latitude, it.longitude) },
        { showImageFullscreen(it) }
    )

    companion object {
        private const val RC_IMAGE_PICKER = 110
        fun intent(context: Context) = Intent(context, AccountActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityAccountBinding = DataBindingUtil.setContentView(this, R.layout.activity_account)
        binding.activity = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        list_history.adapter = adapter

        viewModel.historyList.observe(this, Observer {
            if (it.isEmpty()) {
                binding.hintHistory.setText(R.string.account_msg_empty_history)
            } else {
                binding.hintHistory.setText(R.string.account_hint_history)
                adapter.list = it.reversed()
            }
        })
        viewModel.errorMessage.observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                viewModel.onErrorHandled()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_IMAGE_PICKER && resultCode == Activity.RESULT_OK) {
            val image = ImagePicker.getFirstImageOrNull(data)
            viewModel.uploadPhoto(image.path)
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    fun showImageFullscreen(imageUrl: String) {
        StfalconImageViewer.Builder<String>(this, listOf(imageUrl)) { view, url ->
            Picasso.get().load(url).into(view)
        }.show()
    }

    fun openImagePicker() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.CAMERA_ONLY)
            .single()
            .toolbarImageTitle("Pilih Foto Profil")
            .theme(R.style.AppTheme_CustomImagePicker)
            .start(RC_IMAGE_PICKER)
    }

    private fun openMapsDirection(latitude: Double, longitude: Double) {
        val gmmIntentUri = Uri.parse("google.navigation:q=$latitude,$longitude")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
