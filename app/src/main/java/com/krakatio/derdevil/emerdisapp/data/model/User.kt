package com.krakatio.derdevil.emerdisapp.data.model

import com.google.firebase.database.Exclude

data class User(
    var firstName: String = "",
    var lastName: String = "",
    @get:Exclude var uid: String = ""
)