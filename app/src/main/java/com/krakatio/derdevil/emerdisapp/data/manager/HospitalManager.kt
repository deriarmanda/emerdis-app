package com.krakatio.derdevil.emerdisapp.data.manager

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.util.default

class HospitalManager private constructor() {

    private val mDbRef = FirebaseDatabase.getInstance().getReference("hospitals").orderByChild("type")
    private val mHospitalList = MutableLiveData<List<Hospital>>().default(emptyList())

    companion object {
        @Volatile
        private var instance: HospitalManager? = null

        fun getInstance(): HospitalManager {
            return instance ?: synchronized(this) {
                instance ?: HospitalManager().also { instance = it }
            }
        }
    }

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                val list = arrayListOf<Hospital>()
                for (child in snapshot.children) {
                    val key = child.key
                    val hospital = child.getValue(Hospital::class.java)
                    if (key != null && hospital != null) {
                        hospital.uid = key
                        list.add(hospital)
                    }
                }
                mHospitalList.postValue(list)
            }
        })
    }

    fun getHospitalList(): LiveData<List<Hospital>> = mHospitalList

    fun reloadList(onComplete: (errorMsg: String?) -> Unit) {
        mDbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                onComplete(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list = arrayListOf<Hospital>()
                for (child in snapshot.children) {
                    val key = child.key
                    val hospital = child.getValue(Hospital::class.java)
                    if (key != null && hospital != null) {
                        hospital.uid = key
                        list.add(hospital)
                    }
                }
                mHospitalList.value = list
                onComplete(null)
            }
        })
    }
}