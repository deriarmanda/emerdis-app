package com.krakatio.derdevil.emerdisapp.ui.splash

import android.Manifest
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.ui.auth.AuthActivity
import com.krakatio.derdevil.emerdisapp.ui.home.HomeActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Dexter.withActivity(this).withPermissions(
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        ).withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                if (report.areAllPermissionsGranted()) {
                    val user = FirebaseAuth.getInstance().currentUser
                    if (user == null) onUserNotSignedIn()
                    else onUserSignedIn(user)
                }
            }

            override fun onPermissionRationaleShouldBeShown(
                permissions: MutableList<PermissionRequest>?,
                token: PermissionToken?
            ) {
                token?.continuePermissionRequest()
            }
        }).onSameThread().check()
    }

    private fun onUserSignedIn(user: FirebaseUser) {
        Toast.makeText(this, getString(R.string.home_msg_welcome, user.displayName), Toast.LENGTH_LONG).show()
        startActivity(HomeActivity.intent(this))
        finish()
    }

    private fun onUserNotSignedIn() {
        startActivity(AuthActivity.intent(this))
        finish()
    }
}
