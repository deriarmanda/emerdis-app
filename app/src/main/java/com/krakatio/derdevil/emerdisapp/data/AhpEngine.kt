package com.krakatio.derdevil.emerdisapp.data

import android.location.Location
import android.util.Log
import androidx.lifecycle.Observer
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.krakatio.derdevil.emerdisapp.data.manager.HospitalManager
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.util.TriageResult

class AhpEngine private constructor(
    private val hospitalManager: HospitalManager
) {

    companion object {
        private const val TAG = "AhpEngine"
        private const val KEY_BASE_LEVEL = "base_priority_level"
        private const val KEY_BASE_DISTANCE = "base_priority_distance"
        private const val KEY_BASE_VISITOR = "base_priority_visitor"

        private const val KEY_LEVEL_4 = "level_priority_iv"
        private const val KEY_LEVEL_3 = "level_priority_iii"
        private const val KEY_LEVEL_2 = "level_priority_ii"
        private const val KEY_LEVEL_1 = "level_priority_i"

        private const val KEY_VERY_NEAR = "distance_priority_very_near"
        private const val KEY_NEAR = "distance_priority_near"
        private const val KEY_FAR = "distance_priority_far"
        private const val KEY_VERY_FAR = "distance_priority_very_far"

        private const val KEY_EMPTY = "visitor_priority_empty"
        private const val KEY_HALF = "visitor_priority_half"
        private const val KEY_FULL = "visitor_priority_full"

        @Volatile
        private var instance: AhpEngine? = null

        fun getInstance(hospitalManager: HospitalManager): AhpEngine {
            return instance ?: synchronized(this) {
                instance ?: AhpEngine(hospitalManager).also { instance = it }
            }
        }
    }

    private val redMatrix: HashMap<String, Double> = hashMapOf()
    private val yellowMatrix: HashMap<String, Double> = hashMapOf()
    private val greenMatrix: HashMap<String, Double> = hashMapOf()

    init {
        fetchPriorityMatrix { }
    }

    fun getRecommendations(
        triage: TriageResult,
        location: Location,
        callback: (error: String?, result: ArrayList<Hospital>?) -> Unit
    ) {
        Log.d(TAG, "userLat: ${location.latitude}, userLong: ${location.longitude}")
        hospitalManager.getHospitalList().observeForever(object : Observer<List<Hospital>> {
            override fun onChanged(list: List<Hospital>?) {
                if (list != null && list.isNotEmpty()) {
                    if (redMatrix.isEmpty() || yellowMatrix.isEmpty() || greenMatrix.isEmpty()) {
                        fetchPriorityMatrix { fetchingError ->
                            if (fetchingError != null) callback(fetchingError, null)
                            else {
                                val map = when (triage) {
                                    TriageResult.IMMEDIATE -> redMatrix
                                    TriageResult.DELAYED -> yellowMatrix
                                    else -> greenMatrix
                                }
                                calculateRecommendations(list, map, location) { calculationError, result ->
                                    callback(calculationError, result)
                                    hospitalManager.getHospitalList().removeObserver(this)
                                }
                            }
                        }
                    } else {
                        val map = when (triage) {
                            TriageResult.IMMEDIATE -> redMatrix
                            TriageResult.DELAYED -> yellowMatrix
                            else -> greenMatrix
                        }
                        calculateRecommendations(list, map, location) { error, result ->
                            callback(error, result)
                            hospitalManager.getHospitalList().removeObserver(this)
                        }
                    }
                }
            }
        })
    }

    private fun fetchPriorityMatrix(callback: (error: String?) -> Unit) {
        val dbRef = FirebaseDatabase.getInstance().getReference("ahp")
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                redMatrix.clear()
                yellowMatrix.clear()
                greenMatrix.clear()
                callback(dbError.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children) {
                    val map = when (data.key ?: "green") {
                        "red" -> redMatrix
                        "yellow" -> yellowMatrix
                        else -> greenMatrix
                    }

                    // fetch level's priority vectors
                    val level = data.child("level")
                    val baseLevel = level.child("base_priority").getValue(Double::class.java)
                    if (baseLevel != null) map[KEY_BASE_LEVEL] = baseLevel
                    val level1 = level.child("priority_i").getValue(Double::class.java)
                    if (level1 != null) map[KEY_LEVEL_1] = level1
                    val level2 = level.child("priority_ii").getValue(Double::class.java)
                    if (level2 != null) map[KEY_LEVEL_2] = level2
                    val level3 = level.child("priority_iii").getValue(Double::class.java)
                    if (level3 != null) map[KEY_LEVEL_3] = level3
                    val level4 = level.child("priority_iv").getValue(Double::class.java)
                    if (level4 != null) map[KEY_LEVEL_4] = level4

                    // fetch distance's priority vectors
                    val distance = data.child("distance")
                    val baseDistance = distance.child("base_priority").getValue(Double::class.java)
                    if (baseDistance != null) map[KEY_BASE_DISTANCE] = baseDistance
                    val veryNear = distance.child("priority_very_near").getValue(Double::class.java)
                    if (veryNear != null) map[KEY_VERY_NEAR] = veryNear
                    val near = distance.child("priority_near").getValue(Double::class.java)
                    if (near != null) map[KEY_NEAR] = near
                    val far = distance.child("priority_far").getValue(Double::class.java)
                    if (far != null) map[KEY_FAR] = far
                    val veryFar = distance.child("priority_very_far").getValue(Double::class.java)
                    if (veryFar != null) map[KEY_VERY_FAR] = veryFar

                    // fetch visitor's priority vectors
                    val visitor = data.child("visitor")
                    val baseVisitor = visitor.child("base_priority").getValue(Double::class.java)
                    if (baseVisitor != null) map[KEY_BASE_VISITOR] = baseVisitor
                    val empty = visitor.child("priority_empty").getValue(Double::class.java)
                    if (empty != null) map[KEY_EMPTY] = empty
                    val half = visitor.child("priority_half").getValue(Double::class.java)
                    if (half != null) map[KEY_HALF] = half
                    val full = visitor.child("priority_full").getValue(Double::class.java)
                    if (full != null) map[KEY_FULL] = full
                }
                callback(null)
            }
        })
    }

    private fun calculateRecommendations(
        list: List<Hospital>,
        map: Map<String, Double>,
        userLocation: Location,
        callback: (error: String?, result: ArrayList<Hospital>?) -> Unit
    ) {
        val result = hashMapOf<Hospital, Double>()
        val distances = hashMapOf<String, Double>()
        for (hospital in list) {
            val level = map.getValue(KEY_BASE_LEVEL).times(
                when (hospital.igdLevel) {
                    "IV" -> map.getValue(KEY_LEVEL_4)
                    "III" -> map.getValue(KEY_LEVEL_3)
                    "II" -> map.getValue(KEY_LEVEL_2)
                    else -> map.getValue(KEY_LEVEL_1)
                }
            )
            val visitor = map.getValue(KEY_BASE_VISITOR).times(
                when (hospital.visitors) {
                    in 0L..5L -> map.getValue(KEY_EMPTY)
                    in 6L..10L -> map.getValue(KEY_HALF)
                    else -> map.getValue(KEY_FULL)
                }
            )
            val location = Location("")
            location.latitude = hospital.latitude
            location.longitude = hospital.longitude
            val distanceTemp = userLocation.distanceTo(location)
            val speed = 333.333f
            val time = distanceTemp / speed
            Log.d(TAG, "distance of ${hospital.name}: $distanceTemp with time: $time")
            val distance = map.getValue(KEY_BASE_DISTANCE).times(
                when (time) {
                    in 0f..5f -> map.getValue(KEY_VERY_NEAR)
                    in 6f..10f -> map.getValue(KEY_NEAR)
                    in 15f..30f -> map.getValue(KEY_FAR)
                    else -> map.getValue(KEY_VERY_FAR)
                }
            )

            result[hospital] = level + visitor + distance
            distances[hospital.uid] = distanceTemp.toDouble()
        }
        val recommendations = result.toList().sortedWith(
            Comparator { o1, o2 ->
                val comparator = o2.second.compareTo(o1.second)
                if (comparator != 0) comparator
                else distances[o1.first.uid]!!.compareTo(distances[o2.first.uid]!!)
            }
        )//.asReversed()//.sortedByDescending { it.second }
        recommendations.forEach { Log.d(TAG, "${it.first.name} -> ${it.second}") }
        callback(
            null, arrayListOf(
                recommendations[0].first,
                recommendations[1].first,
                recommendations[2].first
            )
        )
    }
}