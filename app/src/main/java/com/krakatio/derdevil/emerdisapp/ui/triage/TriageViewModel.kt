package com.krakatio.derdevil.emerdisapp.ui.triage

import android.location.Location
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.AhpEngine
import com.krakatio.derdevil.emerdisapp.data.manager.HistoryManager
import com.krakatio.derdevil.emerdisapp.data.model.History
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.util.DateTimeUtils
import com.krakatio.derdevil.emerdisapp.util.TriageResult
import com.krakatio.derdevil.emerdisapp.util.TriageStep
import com.krakatio.derdevil.emerdisapp.util.default
import java.io.File
import java.util.*

class TriageViewModel private constructor(
    private val ahpEngine: AhpEngine,
    private val historyManager: HistoryManager
) : ViewModel() {

    val history = MutableLiveData<History>().default(
        History(
            dateTime = DateTimeUtils.getCurrentDateTimeStringForDb(),
            triage = TriageResult.MINOR.message
        )
    )
    val viewState = MutableLiveData<ViewState>().default(ViewState())
    var userLocation: Location? = null
    private val result = MutableLiveData<AhpResult>()

    fun getResult(): LiveData<AhpResult> = result
    fun setPhotoPath(path: String) {
        history.value = currentHistory().copy(problemUrl = path)
    }

    fun onPositiveBtnClick() {
        when (currentState().step) {
            TriageStep.STEP_1 -> {
                history.value = currentHistory().copy(triage = TriageResult.MINOR.message)
                viewState.value = currentState().copy(
                    page = 1,
                    result = TriageResult.MINOR,
                    statusCardColorRes = TriageResult.MINOR.colorRes,
                    statusTextColorRes = R.color.white
                )
            }
            TriageStep.STEP_2 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_4)
            }
            TriageStep.STEP_3, TriageStep.STEP_4, TriageStep.STEP_7 -> {
                history.value = currentHistory().copy(triage = TriageResult.IMMEDIATE.message)
                viewState.value = currentState().copy(
                    page = 1,
                    result = TriageResult.IMMEDIATE,
                    statusCardColorRes = TriageResult.IMMEDIATE.colorRes,
                    statusTextColorRes = R.color.white
                )
            }
            TriageStep.STEP_5 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_6)
            }
            TriageStep.STEP_6 -> {
                history.value = currentHistory().copy(triage = TriageResult.DELAYED.message)
                viewState.value = currentState().copy(
                    page = 1,
                    result = TriageResult.DELAYED,
                    statusCardColorRes = TriageResult.DELAYED.colorRes,
                    statusTextColorRes = R.color.secondary_text
                )
            }
        }
    }

    fun onNegativeBtnClick() {
        when (currentState().step) {
            TriageStep.STEP_1 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_2)
            }
            TriageStep.STEP_2 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_3)
            }
            TriageStep.STEP_3 -> {
                history.value = currentHistory().copy(triage = TriageResult.DECEASED.message)
                viewState.value = currentState().copy(
                    page = 1,
                    result = TriageResult.DECEASED,
                    statusCardColorRes = TriageResult.DECEASED.colorRes,
                    statusTextColorRes = R.color.white
                )
            }
            TriageStep.STEP_4 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_5)
            }
            TriageStep.STEP_5 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_7)
            }
            TriageStep.STEP_6 -> {
                history.value = currentHistory().copy(triage = TriageResult.IMMEDIATE.message)
                viewState.value = currentState().copy(
                    page = 1,
                    result = TriageResult.IMMEDIATE,
                    statusCardColorRes = TriageResult.IMMEDIATE.colorRes,
                    statusTextColorRes = R.color.white
                )
            }
            TriageStep.STEP_7 -> {
                viewState.value = currentState().copy(step = TriageStep.STEP_6)
            }
        }
    }

    fun calculateResult() {
        viewState.value = currentState().copy(isLoading = true)

        val url = currentHistory().problemUrl
        if (url.isEmpty()) countAndSaveResult(url)
        else {
            // upload image condition
            val user = FirebaseAuth.getInstance().currentUser
            if (user != null) {
                var timestamp = Date().time.toString()
                timestamp = timestamp.substring(timestamp.length - 10, timestamp.length)

                /*val compressed = ImageCompressor.compress(
                    url,
                    if (url.endsWith(".png", ignoreCase = true)) Bitmap.CompressFormat.PNG
                    else Bitmap.CompressFormat.JPEG
                )*/

                val storageRef = FirebaseStorage.getInstance().getReference("image/${user.uid}/$timestamp")
                storageRef.putFile(Uri.fromFile(File(url)))//.putBytes(compressed)
                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                        if (!task.isSuccessful) {
                            task.exception?.let {
                                throw it
                            }
                        }
                        return@Continuation storageRef.downloadUrl
                    }).addOnCompleteListener { uploadTask ->
                        if (uploadTask.isSuccessful) {
                            val uploadedUri = uploadTask.result!!
                            countAndSaveResult(uploadedUri.toString())
                        } else {
                            viewState.value = currentState().copy(
                                isLoading = false,
                                errorMessage = uploadTask.exception?.message!!
                            )
                        }
                    }
            } else {
                viewState.value = currentState().copy(
                    isLoading = false,
                    errorMessage = "Anda harus melakukan login terlebih dahulu!"
                )
            }
        }
    }

    fun onErrorHandled() {
        viewState.value = currentState().copy(errorMessage = null)
    }

    private fun countAndSaveResult(url: String) {
        while (userLocation == null);
        userLocation?.let {
            ahpEngine.getRecommendations(currentState().result, it) { ahpError, ahpResult ->
                if (ahpError != null || ahpResult == null) {
                    viewState.value = currentState().copy(isLoading = false, errorMessage = ahpError)
                } else {
                    val currentHistory = currentHistory().copy(
                        problemUrl = url,
                        uidHospital = ahpResult[0].uid,
                        hospital = ahpResult[0]
                    )
                    historyManager.saveHistory(currentHistory) { savedHistory, error ->
                        viewState.value = currentState().copy(isLoading = false, errorMessage = error)
                        if (error == null) {
                            result.value = AhpResult(savedHistory, ahpResult)
                        }
                    }
                }
            }
        }
    }

    private fun currentHistory() = history.value!!
    private fun currentState() = viewState.value!!

    data class ViewState(
        var page: Int = 0,
        var step: TriageStep = TriageStep.STEP_1,
        var result: TriageResult = TriageResult.MINOR,
        var statusCardColorRes: Int = R.color.green,
        var statusTextColorRes: Int = R.color.white,
        var isLoading: Boolean = false,
        var errorMessage: String? = null
    )

    data class AhpResult(
        var history: History,
        var hospitals: ArrayList<Hospital>
    )

    class Factory(
        private val ahpEngine: AhpEngine,
        private val historyManager: HistoryManager
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return TriageViewModel(ahpEngine, historyManager) as T
        }
    }
}