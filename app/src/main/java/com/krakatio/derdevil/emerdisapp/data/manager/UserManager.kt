package com.krakatio.derdevil.emerdisapp.data.manager

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class UserManager private constructor() {

    private val mAuth = FirebaseAuth.getInstance()

    companion object {
        @Volatile
        private var instance: UserManager? = null

        fun getInstance(): UserManager {
            return instance ?: synchronized(this) {
                instance ?: UserManager().also { instance = it }
            }
        }
    }

    fun signIn(email: String, password: String, callback: (errorMsg: String?) -> Unit) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) callback(null)
            else callback(it.exception?.message ?: "Terjadi kesalahan, silahkan coba lagi.")
        }
    }

    fun signUp(fullname: String, email: String, password: String, callback: (errorMsg: String?) -> Unit) {
        val profileChangeRequest = UserProfileChangeRequest.Builder()
            .setDisplayName(fullname)
            .build()
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { signUpTask ->
            val user = signUpTask.result?.user
            if (signUpTask.isSuccessful && user != null) {
                user.updateProfile(profileChangeRequest).addOnCompleteListener { updateTask ->
                    if (updateTask.isSuccessful) callback(null)
                    else callback(updateTask.exception?.message ?: "Terjadi kesalahan, silahkan coba lagi.")
                }
            } else callback(signUpTask.exception?.message ?: "Terjadi kesalahan, silahkan coba lagi.")
        }

    }
}