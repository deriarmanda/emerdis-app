package com.krakatio.derdevil.emerdisapp.ui.home.information

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.krakatio.derdevil.emerdisapp.data.model.Information
import com.krakatio.derdevil.emerdisapp.util.default

class InformationViewModel : ViewModel() {

    private val dbRef = FirebaseDatabase.getInstance().getReference("info")
    private val list = MutableLiveData<List<Information>>().default(emptyList())
    private val viewState = MutableLiveData<ViewState>().default(ViewState())

    init {
        fetchList()
    }

    fun getList(): LiveData<List<Information>> = list
    fun getViewState(): LiveData<ViewState> = viewState

    fun fetchList() {
        viewState.value = currentState().copy(isLoading = true)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                viewState.value = currentState().copy(
                    isLoading = false,
                    errorMessage = dbError.message
                )
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val addedList = arrayListOf<Information>()
                for (childs in snapshot.children) {
                    val item = childs.getValue(Information::class.java)
                    if (item != null) addedList.add(item)
                }
                list.value = addedList
                viewState.value = currentState().copy(isLoading = false)
            }

        })
    }

    fun onErrorHandled() {
        viewState.value = currentState().copy(errorMessage = null)
    }

    private fun currentState() = viewState.value!!

    data class ViewState(
        var isLoading: Boolean = false,
        var errorMessage: String? = null
    )
}