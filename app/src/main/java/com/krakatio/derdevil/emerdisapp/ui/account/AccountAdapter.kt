package com.krakatio.derdevil.emerdisapp.ui.account

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.emerdisapp.data.model.History
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.databinding.ItemHistoryBinding

class AccountAdapter(
    private val onVisitButtonClick: (hospital: Hospital) -> Unit,
    private val onImageClick: (imageUrl: String) -> Unit
) : RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    var list = listOf<History>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    private var expandedPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemHistoryBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val history = list[position]
        holder.binding.history = history
        holder.binding.isExpanded = position == expandedPosition
        holder.binding.topLineFlag = position != 0
        holder.binding.bottomLineFlag = position != (list.size - 1)

        holder.binding.buttonVisit.setOnClickListener { onVisitButtonClick(history.hospital) }
        holder.binding.imageCondition.setOnClickListener { onImageClick(history.problemUrl) }
        holder.binding.container.setOnClickListener {
            holder.binding.isExpanded = holder.binding.isExpanded?.not()
        }
    }

    inner class ViewHolder(val binding: ItemHistoryBinding) : RecyclerView.ViewHolder(binding.root)
}