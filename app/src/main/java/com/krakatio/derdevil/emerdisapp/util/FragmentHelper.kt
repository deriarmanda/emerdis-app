package com.krakatio.derdevil.emerdisapp.util

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

//auth.AuthActivity
const val FRAGMENT_SIGN_IN = "fragment_tag_signin"
const val FRAGMENT_SIGN_UP = "fragment_tag_signup"

// home.HomeActivity
const val FRAGMENT_WELCOME = "fragment_tag_welcome"
const val FRAGMENT_MAPS = "fragment_tag_maps"
const val FRAGMENT_EXPLORE = "fragment_tag_explore"
const val FRAGMENT_INFO = "fragment_tag_information"
const val FRAGMENT_SEARCH = "fragment_tag_search"

fun AppCompatActivity.replaceFragmentByTag(
    @IdRes containerId: Int,
    fragment: Fragment,
    tag: String,
    allowingStateLoss: Boolean = false
) {
    val fragTransaction = supportFragmentManager
        .beginTransaction()
        .replace(containerId, fragment, tag)

    if (allowingStateLoss) fragTransaction.commitAllowingStateLoss()
    else fragTransaction.commit()
}