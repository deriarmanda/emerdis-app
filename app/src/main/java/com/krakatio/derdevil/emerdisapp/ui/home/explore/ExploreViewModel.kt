package com.krakatio.derdevil.emerdisapp.ui.home.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krakatio.derdevil.emerdisapp.data.manager.HospitalManager
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.util.default

class ExploreViewModel(
    private val hospitalManager: HospitalManager
) : ViewModel() {

    private val list = hospitalManager.getHospitalList()
    private val viewState = MutableLiveData<ViewState>().default(ViewState())

    init {
        reloadList()
    }

    fun getList(): LiveData<List<Hospital>> = list
    fun getViewState(): LiveData<ViewState> = viewState

    fun reloadList() {
        viewState.value = currentState().copy(isLoading = true)
        hospitalManager.reloadList {
            viewState.value = currentState().copy(
                isLoading = false,
                errorMessage = it
            )
        }
    }

    fun onErrorHandled() {
        viewState.value = currentState().copy(errorMessage = null)
    }

    private fun currentState() = viewState.value!!

    data class ViewState(
        var isLoading: Boolean = false,
        var errorMessage: String? = null
    )

    class Factory(
        private val hospitalManager: HospitalManager
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ExploreViewModel(hospitalManager) as T
        }
    }
}
