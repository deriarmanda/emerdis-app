package com.krakatio.derdevil.emerdisapp.util

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.krakatio.derdevil.emerdisapp.R
import com.squareup.picasso.Picasso

@BindingAdapter("isShown")
fun View.setVisibility(isShown: Boolean) {
    this.visibility = if (isShown) View.VISIBLE else View.GONE
}

@BindingAdapter("isHistoryExpanded", "contentHistory", requireAll = true)
fun View.setVisibilityForHistory(isExpanded: Boolean, content: String) {
    this.visibility = if (isExpanded && content.isNotBlank()) View.VISIBLE else View.GONE
}

@BindingAdapter("historyFormattedText")
fun TextView.setHistoryFormattedText(dateTime: String) {
    text = DateTimeUtils.parseForHistory(dateTime)
}

@BindingAdapter("remoteSrc")
fun ImageView.loadImageFromUrl(url: String) {
    if (url.isNotBlank()) {
        Picasso.get()
            .load(url)
            .placeholder(R.drawable.img_placeholder)
            .error(R.drawable.img_placeholder)
            .resize(width, height)
            .into(this)
    }
}

@BindingAdapter("remoteSrcWithPlaceholder")
fun ImageView.loadImageFromUrlWithPlaceholder(url: String) {
    Picasso.get()
        .load(url)
        .placeholder(R.drawable.ic_account_circle_primary_96dp)
        .error(R.drawable.ic_account_circle_primary_96dp)
        .into(this)
}