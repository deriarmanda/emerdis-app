package com.krakatio.derdevil.emerdisapp.ui.account

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.krakatio.derdevil.emerdisapp.data.manager.HistoryManager
import com.krakatio.derdevil.emerdisapp.util.default
import java.io.File

class AccountViewModel private constructor(
    private val historyManager: HistoryManager
) : ViewModel() {

    val user: FirebaseUser
    val historyList = historyManager.getHistorylList()
    val imageUrl: MutableLiveData<String>
    val isLoading = MutableLiveData<Boolean>().default(false)
    val errorMessage = MutableLiveData<String?>().default(null)

    init {
        val auth = FirebaseAuth.getInstance()
        user = auth.currentUser!!
        imageUrl = MutableLiveData(user.photoUrl.toString())
        reloadHistory()
    }

    fun uploadPhoto(path: String) {
        isLoading.postValue(true)
        val storageRef = FirebaseStorage.getInstance().getReference("image/${user.uid}")
        storageRef.putFile(Uri.fromFile(File(path)))
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation storageRef.downloadUrl
            }).addOnCompleteListener { uploadTask ->
                if (uploadTask.isSuccessful) {
                    val uploadedUri = uploadTask.result!!
                    val profileChangeRequest = UserProfileChangeRequest.Builder()
                        .setPhotoUri(uploadedUri)
                        .build()
                    user.updateProfile(profileChangeRequest).addOnCompleteListener { updateTask ->
                        if (updateTask.isSuccessful) {
                            errorMessage.value = null
                            imageUrl.value = user.photoUrl.toString()
                        } else {
                            errorMessage.value = updateTask.exception?.message!!
                        }
                        isLoading.value = false
                    }
                } else {
                    errorMessage.value = uploadTask.exception?.message!!
                    isLoading.value = false
                }
            }
    }

    fun onErrorHandled() {
        errorMessage.value = null
    }

    private fun reloadHistory() {
        isLoading.value = true
        historyManager.reloadList {
            isLoading.value = false
            errorMessage.value = it
        }
    }

    class Factory(
        private val historyManager: HistoryManager
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AccountViewModel(historyManager) as T
        }
    }
}