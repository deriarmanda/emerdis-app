package com.krakatio.derdevil.emerdisapp.util

import com.krakatio.derdevil.emerdisapp.R

enum class TriageResult(
    val message: String,
    val colorRes: Int
) {
    IMMEDIATE("Gawat Darurat", R.color.red),
    DELAYED("Gawat Tidak Darurat", R.color.yellow),
    MINOR("Tidak Gawat Darurat", R.color.green),
    DECEASED("Meninggal", R.color.primary_text)
}