package com.krakatio.derdevil.emerdisapp.ui.auth.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krakatio.derdevil.emerdisapp.data.manager.UserManager
import com.krakatio.derdevil.emerdisapp.util.default

class SignInViewModel private constructor(
    private val userManager: UserManager
) : ViewModel() {

    val email = MutableLiveData<String>().default("")
    val password = MutableLiveData<String>().default("")
    val isLoading = MutableLiveData<Boolean>().default(false)
    private val signInStatus = MutableLiveData<SignInStatus>().default(SignInStatus())

    fun getSignInStatus(): LiveData<SignInStatus> = signInStatus

    fun doSignIn() {
        isLoading.value = true
        userManager.signIn(email.value!!, password.value!!) {
            isLoading.value = false
            signInStatus.value = currentStatus().copy(
                isSuccess = it == null,
                errorMessage = it
            )
        }
    }

    private fun currentStatus() = signInStatus.value!!

    data class SignInStatus(
        var isSuccess: Boolean = false,
        var errorMessage: String? = null
    )

    class Factory(
        private val userManager: UserManager
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SignInViewModel(userManager) as T
        }
    }
}

