package com.krakatio.derdevil.emerdisapp.ui.home.search


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.krakatio.derdevil.emerdisapp.R
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.ui.hospital.HospitalActivity
import com.krakatio.derdevil.emerdisapp.util.Injectors
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * A simple [Fragment] subclass.
 *
 */
class SearchFragment : Fragment() {

    companion object {
        fun instance() = SearchFragment()
    }

    private lateinit var hospitalList: List<Hospital>
    private var onSearchCompleteListener: OnSearchCompleteListener? = null
    private val adapter = SearchAdapter {
        startActivity(HospitalActivity.intent(requireContext(), it))
        onSearchCompleteListener?.onSearchComplete()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnSearchCompleteListener) onSearchCompleteListener = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_search_result.adapter = adapter

        val hospitalManager = Injectors.getHospitalManager()
        hospitalManager.getHospitalList().observe(viewLifecycleOwner, Observer { hospitalList = it })
    }

    override fun onDetach() {
        super.onDetach()
        onSearchCompleteListener = null
    }

    fun processQuery(query: String) {
        val list = arrayListOf<Hospital>()
        val queryLowerCase = query.toLowerCase()
        for (hospital in hospitalList) {
            val name = hospital.name.toLowerCase()
            if (name.contains(queryLowerCase)) list.add(hospital)
        }
        adapter.list = list
    }

    interface OnSearchCompleteListener {
        fun onSearchComplete()
    }
}
