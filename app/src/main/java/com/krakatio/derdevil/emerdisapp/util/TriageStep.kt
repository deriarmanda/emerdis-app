package com.krakatio.derdevil.emerdisapp.util

import com.krakatio.derdevil.emerdisapp.R

enum class TriageStep(
    val questionStringRes: Int,
    val questionDrawableRes: Int,
    val positiveBtnRes: Int,
    val negativeBtnRes: Int
) {
    STEP_1(
        R.string.triage_question_step_1,
        R.drawable.img_triage_step_1,
        R.string.triage_action_can,
        R.string.triage_action_cant
    ),
    STEP_2(
        R.string.triage_question_step_2,
        R.drawable.img_triage_step_2,
        R.string.triage_action_yes,
        R.string.triage_action_no
    ),
    STEP_3(
        R.string.triage_question_step_3,
        R.drawable.img_triage_step_3,
        R.string.triage_action_yes,
        R.string.triage_action_no
    ),
    STEP_4(
        R.string.triage_question_step_4,
        R.drawable.img_triage_step_4,
        R.string.triage_action_yes,
        R.string.triage_action_no
    ),
    STEP_5(
        R.string.triage_question_step_5,
        R.drawable.img_triage_step_5,
        R.string.triage_action_present,
        R.string.triage_action_absent
    ),
    STEP_6(
        R.string.triage_question_step_6,
        R.drawable.img_triage_step_6,
        R.string.triage_action_can,
        R.string.triage_action_cant
    ),
    STEP_7(
        R.string.triage_question_step_7,
        R.drawable.img_triage_step_7,
        R.string.triage_action_yes,
        R.string.triage_action_no
    )
}