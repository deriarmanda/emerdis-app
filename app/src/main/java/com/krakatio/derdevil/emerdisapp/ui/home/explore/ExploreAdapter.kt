package com.krakatio.derdevil.emerdisapp.ui.home.explore

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.emerdisapp.data.model.Hospital
import com.krakatio.derdevil.emerdisapp.databinding.ItemHospitalBinding

class ExploreAdapter(
    private val onItemClick: (hospital: Hospital) -> Unit
) : RecyclerView.Adapter<ExploreAdapter.ViewHolder>() {

    var list = listOf<Hospital>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemHospitalBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hospital = list[position]
        holder.binding.hospital = hospital
        holder.binding.typeFlag = position == 0 || hospital.type != list[position - 1].type
        holder.binding.container.setOnClickListener { onItemClick(hospital) }
    }

    inner class ViewHolder(val binding: ItemHospitalBinding) : RecyclerView.ViewHolder(binding.root)
}