package com.krakatio.derdevil.emerdisapp.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude

data class History(
    var dateTime: String = "",
    var triage: String = "",
    var problem: String = "",
    var problemUrl: String = "",
    var uidHospital: String = "",
    @get:Exclude var uid: String = "",
    @get:Exclude var hospital: Hospital = Hospital()
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readParcelable(Hospital::class.java.classLoader) ?: Hospital()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dateTime)
        parcel.writeString(triage)
        parcel.writeString(problem)
        parcel.writeString(problemUrl)
        parcel.writeString(uidHospital)
        parcel.writeString(uid)
        parcel.writeParcelable(hospital, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<History> {
        override fun createFromParcel(parcel: Parcel): History {
            return History(parcel)
        }

        override fun newArray(size: Int): Array<History?> {
            return arrayOfNulls(size)
        }
    }
}